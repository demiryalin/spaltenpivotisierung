#!/usr/bin/python

def l_matrix(x,y,strichmatrix,schrittenzahl):

 l_matrix = [[1]*y for i in range(x)]

 for i in range(0,x):
  for j in range(0,y):
   if i!=j:
    l_matrix[i][j] = 0

 for i in range(schrittenzahl,x):
  l_matrix[i][schrittenzahl-1] = (-float(strichmatrix[i][schrittenzahl-1])/(float(strichmatrix[schrittenzahl-1][schrittenzahl-1])))

 return l_matrix
