#!/usr/bin/python

def p_matrix(ind,pivotelement,x,y,schrittenzahl):
 einheitsmatrix = [[1]*y for i in range(x)]
 p_matrix = [[0]*y for i in range(x)]
 lis = []
 lis2 = []
 einheitsmatrix = [[1]*y for i in range(x)]
 for i in range(0,x):
  for j in range(0,y):
   if i != j:
    einheitsmatrix[i][j] = 0

 if float(ind) == (schrittenzahl-1):
  p_matrix = einheitsmatrix
  return p_matrix

 else:
  for i in range(0,x):
   lis.append(einheitsmatrix[ind][i])
   lis2.append(einheitsmatrix[schrittenzahl-1][i])

  for i in range(0,x-1):
   einheitsmatrix[schrittenzahl-1][i] = lis[i]
   einheitsmatrix[ind][i] = lis2[i]
  p_matrix = einheitsmatrix

  return p_matrix
