#!/usr/bin/python
import matrixerstellen
import getpivotelement
import getpmatrix
import getstrich
import getlmatrix
import naechstezwischenmatrix



print ('geben Sie die groesse der Matrix ein')
x = input(" geben Sie Zeilengroesse ein: ")
y = input(" geben Sie Spaltengroesse ein: ")

while x!=y:
 print "geben Sie bitte eine symmetrische matrix ein!!"
 x = input(" geben Sie Zeilengroesse ein: ")
 y = input(" geben Sie Spaltengroesse ein: ")

matrix = matrixerstellen.matrix_erstellen(x,y)
zwischen_matrix = matrix
schrittenzahl = x

lis = []

for i in range(1,schrittenzahl):
 lis.append(getpivotelement.get_pivot(zwischen_matrix,x,0,i)[0])
 lis.append(getpivotelement.get_pivot(zwischen_matrix,x,0,i)[1])
 pivotelement = lis[0]
 ind = lis[1]
 p_matrix = getpmatrix.p_matrix(ind,pivotelement,x,y,i)

 print p_matrix

 strichmatrix = getstrich.getstrich(p_matrix,zwischen_matrix,x,y)

 print strichmatrix

 l_matrix = getlmatrix.l_matrix(x,y,strichmatrix,i)

 print l_matrix

 zwischen_matrix = naechstezwischenmatrix.naechstez_matrix(l_matrix,strichmatrix,x,y)

 print zwischen_matrix

 lis = []
