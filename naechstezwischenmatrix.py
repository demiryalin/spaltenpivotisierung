#!/usr/bin/python

def naechstez_matrix(l_matrix,strichmatrix,x,y):
 result = [[0]*y for i in range(x)]
 result = [[sum(a*b for a,b in zip(l_matrix_row,strichmatrix_col)) for strichmatrix_col in zip(*strichmatrix)] for l_matrix_row in l_matrix]
 return result
